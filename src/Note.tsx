import React, { useState } from "react";
import './Note.css';

const Note: React.FC = () => {

    const [note, setNote] = useState("");
    const [notes, setNotes] = useState<string[]>([
        "make a note-taking application",
        "buy milk",
        "pay the rent",
        "BUY TICKETS TO THE CONCERT!!!",
        "fix the bike"
    ]);

    const updateNotes = () => {
        const updatedNotes = [...notes];
        setNotes([...updatedNotes, note]);
        setNote("");
    }

    const deleteNote = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        const id = Number(event.currentTarget.id) - 1;
        const updatedNotes = [...notes];
        updatedNotes.splice(id, 1);
        setNotes(updatedNotes);
    }

    return (
        <div className="container">
            <div className="form">
                <input value={note} onChange={(event) => setNote(event.target.value)} type="text" maxLength={50} defaultValue="" />
                <button onClick={updateNotes}>
                    Add new note
            </button>
            </div>
            <div className="notes">
                {notes.map((note, index) => {
                    return (
                        <div className="note" key={note.substr(0, 3) + index}>
                            <div className="note_header">
                                <h3>Note #{index + 1}</h3>
                                <div className="delete" id={(index + 1).toString()} onClick={deleteNote}>X</div>
                            </div>
                            <div className="note_text">
                                <p>{note}</p>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

export default Note;