import React from 'react';
import './App.css';
import Note from './Note';

const App: React.FC = () => {
   return (
      <div>
         <nav className="nav">
            <h1>NOTE-TAKING APP</h1>
         </nav>
         <Note />
      </div>
   );
}

export default App;